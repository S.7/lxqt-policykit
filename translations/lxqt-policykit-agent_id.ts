<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id">
<context>
    <name>LXQtPolicykit::PolicykitAgent</name>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <location filename="../src/policykitagent.cpp" line="174"/>
        <source>PolicyKit Information</source>
        <translation>Informasi PolicyKit</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <source>Another authentication is in progress. Please try again later.</source>
        <translation>Perizinan lain sedang berlangsung. Silahkan coba lagi nanti.</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization Failed</source>
        <translation>Perizinan gagal</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization failed for some reason</source>
        <translation>Perizinan gagal karena beberapa alasan</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="167"/>
        <source>PolicyKit Error</source>
        <translation>Kesalahan PolicyKit</translation>
    </message>
</context>
<context>
    <name>PolicykitAgentGUI</name>
    <message>
        <location filename="../src/policykitagentgui.ui" line="14"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="100"/>
        <source>Authentication Required</source>
        <translation>Autentikasi Diperlukan</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="55"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="101"/>
        <source>Identity:</source>
        <translation>Identitas:</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="65"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="102"/>
        <source>Password:</source>
        <translation>Kata sandi:</translation>
    </message>
</context>
</TS>
