<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>LXQtPolicykit::PolicykitAgent</name>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <location filename="../src/policykitagent.cpp" line="174"/>
        <source>PolicyKit Information</source>
        <translation>Informazioni PolicyKit</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <source>Another authentication is in progress. Please try again later.</source>
        <translation>E&apos; in corso un altra autenticazione. Riprova più tardi.</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization Failed</source>
        <translation>Autorizzazione non riuscita</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization failed for some reason</source>
        <translation>Autorizzazione non riuscita per alcune ragioni</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="167"/>
        <source>PolicyKit Error</source>
        <translation>Errore di PolicyKit</translation>
    </message>
</context>
<context>
    <name>PolicykitAgentGUI</name>
    <message>
        <location filename="../src/policykitagentgui.ui" line="14"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="100"/>
        <source>Authentication Required</source>
        <translation>Autenticazione richiesta</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="55"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="101"/>
        <source>Identity:</source>
        <translation>Identità:</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="65"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="102"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
</context>
</TS>
