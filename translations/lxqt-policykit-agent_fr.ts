<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>LXQtPolicykit::PolicykitAgent</name>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <location filename="../src/policykitagent.cpp" line="174"/>
        <source>PolicyKit Information</source>
        <translation>Information sur PolicyKit</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <source>Another authentication is in progress. Please try again later.</source>
        <translation>Une autre authentification est en cours. Veuillez réessayer plus tard.</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization Failed</source>
        <translation>Échec de l&apos;autorisation</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization failed for some reason</source>
        <translation>L&apos;autorisation a échoué pour une raison quelconque</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="167"/>
        <source>PolicyKit Error</source>
        <translation>Erreur de PolicyKit</translation>
    </message>
</context>
<context>
    <name>PolicykitAgentGUI</name>
    <message>
        <location filename="../src/policykitagentgui.ui" line="14"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="100"/>
        <source>Authentication Required</source>
        <translation>Authentification requise</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="55"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="101"/>
        <source>Identity:</source>
        <translation>Identité :</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="65"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="102"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
</context>
</TS>
