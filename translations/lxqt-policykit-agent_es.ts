<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>LXQtPolicykit::PolicykitAgent</name>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <location filename="../src/policykitagent.cpp" line="174"/>
        <source>PolicyKit Information</source>
        <translation>Información de PolicyKit</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="81"/>
        <source>Another authentication is in progress. Please try again later.</source>
        <translation>Otra autentificación está en progreso. Inténtelo de nuevo más tarde.</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization Failed</source>
        <translation>La autorización ha fallado</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="151"/>
        <source>Authorization failed for some reason</source>
        <translation>La autorización ha fallado por alguna razón</translation>
    </message>
    <message>
        <location filename="../src/policykitagent.cpp" line="167"/>
        <source>PolicyKit Error</source>
        <translation>Error de PolicyKit</translation>
    </message>
</context>
<context>
    <name>PolicykitAgentGUI</name>
    <message>
        <location filename="../src/policykitagentgui.ui" line="14"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="100"/>
        <source>Authentication Required</source>
        <translation>Autenticación requerida</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="55"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="101"/>
        <source>Identity:</source>
        <translation>Identidad:</translation>
    </message>
    <message>
        <location filename="../src/policykitagentgui.ui" line="65"/>
        <location filename="../build/lxqt-policykit-agent_autogen/include/ui_policykitagentgui.h" line="102"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
</context>
</TS>
